# 1.0.0 (2020-12-15)


### Bug Fixes

* update package.json ([ded4e99](https://gitlab.com/depixy/middleware-env/commit/ded4e996d598427bd9ec7bbdfc61c28e2eff1e97))


### Features

* initial commit ([488df4a](https://gitlab.com/depixy/middleware-env/commit/488df4a3c12a6f28d597334c430729f9f0732313))
