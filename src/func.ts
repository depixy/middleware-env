import _ from "lodash";

import { EnvUtils } from "./type";

const toInt = (value: string): number => parseInt(value, 10);
const toFloat = (value: string): number => parseFloat(value);
const toBool = (value: string): boolean => value.toLowerCase() === "true";
const toString = (value: string): string => value;

const parseEnv = <T>(parseFn: (value: string) => T) => (
  key: string,
  defaultValue: T
): T => {
  if (!_.has(process.env, key)) {
    return defaultValue;
  }
  const value = process.env[key];
  if (_.isNil(value)) {
    return defaultValue;
  }
  return parseFn(value);
};

export const env: EnvUtils = {
  int: parseEnv(toInt),
  float: parseEnv(toFloat),
  bool: parseEnv(toBool),
  string: parseEnv(toString)
};
