import type { Middleware } from "koa";

import { env } from "./func";

export const middleware = async (): Promise<Middleware> => async (
  ctx,
  next
) => {
  ctx.env = env;
  await next();
};

export default middleware;
